﻿<%@ Control Language="C#" AutoEventWireup="true" %>
<%@ Register Src="~/_org/_intro_/heading/H1.ascx" TagPrefix="uc1" TagName="H1" %>
<%@ Register Src="~/fund/_donate_/Mobile.ascx" TagPrefix="uc1" TagName="Mobile" %>
<%@ Register Src="~/fund/_donate_/Qr.ascx" TagPrefix="uc1" TagName="Qr" %>




<section>
	<uc1:H1 runat="server" ID="H1" />

	<p>
		我认为计算机语言是承载、传递科技知识的最好形式，简洁严谨、去伪存真、让伪科学无处藏身；而依靠信息技术运行测试和可视化又能让学习变得有趣。
	</p>
	<p>
		我们研发的知识代数将梳理各个知识库的关系，为学习者规划路径，做到不重不漏，能大大提高学习效率，从而能够攀越更高科技高峰。我们研发的科教领域计算库都将开源，为教育、生产服务。
	</p>
	<p>
		我正在筹办零空科教基金会(<a href="http://nilnul.org">Nilnul.Org
		</a>
		)，支持和维护我们的开源软件以及其在教育领域的研发和应用，倡导以编程学习科技知识。
	</p>


	<section>
		<h1>在编程中学习
		</h1>
		<p>
			为介绍我们的工作和体现我们的理念，接下来我就写一系列这方面的文章：
		</p>
		<ol>
			<li><a href="http://nilnul.com/app_/nilnul._express_._compose_/Loader1.aspx?url=/app_/nilnul.num._quotient_.KEN/eg_/Big.ascx&amp;title=%e9%9b%b6%e7%a9%ba%e7%b3%bb%e7%bb%9f%e6%98%af%e6%80%8e%e4%b9%88%e5%a4%84%e7%90%86%e5%a4%a7%e5%a4%a7%e5%a4%a7%e6%9c%89%e7%90%86%e6%95%b0%e7%9a%84%ef%bc%9f">零空系统是怎么处理大、大、大有理数的</a></li>

			<li>零空工程师是怎样用区间套住一个个实数的</li>
			<li>电脑字体，中学几何课本遗漏的一章
			</li>
			<li>零空对代数公式的处理--写给中学生们
			</li>
			<li>三维不过点线面，自己编写一个立体几何形状或者设计家装吧！</li>
			<li>如果你理解不了四元数怎么处理三维旋转，那你得看看我写的这篇文章</li>
			<li>...</li>
		</ol>
		<p>
			（写完的会把上面文章标题变成链接，敬请点击查看）。
		</p>
		<p>
			您也可考虑加入我们的微信群，在群里可以向我提问（如果有必要，我也会开直播）。就像我在前面文章“零空系统是怎么处理大、大、大有理数的”里那样，我会告诉你如何采用编程方法解决问题，并在实现中于必要时查找资料。
		</p>
		<uc1:Qr runat="server" ID="Qr" />
		<p>
			如果二维码过期，您也可以通过 <a href="mailto:wangyoutian@msn.com">wangyoutian@msn.com</a> 联系我。
		</p>
	</section>

	<section>
		<h1>请支持我们</h1>
		<p>
			如果您想支持我们，请考虑以下方式：
		</p>
		<ol>
			<li>加入我们的志愿者队伍</li>
			<li>捐赠给开发者或者赞助零空公益基金会（筹）</li>
			<li>转发我们的信息</li>
		</ol>
		<uc1:Mobile runat="server" ID="Mobile" />

	</section>

</section>






