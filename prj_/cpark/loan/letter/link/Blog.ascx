﻿<%@ Control Language="C#" AutoEventWireup="true"  %>
<%@ Register Src="~/prj_/cpark/loan/letter/Link.ascx" TagPrefix="uc1" TagName="Link" %>

<section>
	<h1>
		给华夏银行的信
	</h1>
	<p>
		在购房过程中，华夏银行和我分别向房屋销售方支付了19万整和20余万元。我们双方是该房屋交易中出钱最多的双方，也是承受风险最大的双方。
	</p>
	<p>
		由于房屋交易因江夏区政府违反了武汉市和江夏区文件，而处于撤销状态，我在此前近十年里从等待到奔走，而从未拖欠一月还贷。但现已无力支撑，因此我请求华夏银行 利与共、害与共，共同承担风险，共同奔走呼告，要求江夏区政府立即纠正违反法规的行为，并赔偿损失。
	</p>
	<p>
		我按照华夏银行网站里“联系方式”（https://www.hxb.com.cn/jrhx/lxwm/lxfs/index.shtml）找到了其邮箱地址：xfjb@hxb.com.cn，并给华夏银行发送了如下内容：
	</p>
	<p>
		<uc1:Link runat="server" ID="Link" />
	</p>
	<p>
		我将在几天后，给华夏银行致电，与他们商议如何共同维护双方的合法权益。
	</p>
</section>
